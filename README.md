
CONTENTS OF THIS FILE:
---------------------
   
 * Introduction
 * Requirements
 * Installation 
 * Configuration
 * Configuration

Introduction
------------

The workbench_preview_all module adds a tab on the node view page.
It allows for an editor/admin with the permission "view preview page",
to display a node page in it's most recent revision, 
that being 'published' or otherwise.
 All referenced nodes, field collections, 
 or paragraphs are displayed in their latest revisions as well.
 ####The module hides: 
 Toolbar, shortcut, contextual links, node tabs (local tasks)

Requirements
------------

This module is made for Drupal 7 and requires the 
node and workbench moderation modules.

Installation
------------

Install as you would normally install a contributed Drupal module.
See:
https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

Configuration
-------------

The module has no menu or modifiable settings. 
There is no configuration. When enabled, 
the module will add a tab on the node view page.

Limitations
-----------

The module should be able to cover most use cases 
where content with moderation is used, although there 
is a certain limit when it comes to embed views
in nodes. When you embed a view which displays content, 
the content itself should be displayed in it's 
latest revision. The module, however, does not affect the
 view query/result, so any unpublished content
might not display in the view. Make sure you handle your
 view filters in regards to your current use case.
